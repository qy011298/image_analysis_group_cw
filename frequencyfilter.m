% Filter an image, with periodic ripple, in the Fourier domain.
% Band stop filter in the Frequency domain to remove periodic noise

clc;    % Clear the command window.
close all;  % Close all figures (except those of imtool.)
clear;  % Erase all existing variables. Or clearvars if you want.
workspace;  % Make sure the workspace panel is showing.
fontSize = 8;

% Read the image.
fullFileName = 'PandaNoise.bmp';
grayImage = imread(fullFileName);
subplot(2, 3, 1); % placing the Original Image to view
imshow(grayImage, []);
set(gcf, 'Name', ['Results for ' fullFileName]);
title('Noisy Image', 'FontSize', fontSize);
set(gcf, 'units','normalized','outerposition',[0 0 1 1]); % Maximize figure.

% Compute the 2D fft.
frequencyImage = fftshift(fft2(grayImage));
% Take log of the magnitude so we can see in display.
amImage = log(abs(frequencyImage));
subplot(2, 3, 2);
imshow(amImage, []);
caption = sprintf('high spikes due to periodic noise');%Display the high frequency spikes
title(caption, 'FontSize', fontSize);


% Find the location of the  spikes.
amplitudeThreshold = 10.9;
brightSpikes = amImage > amplitudeThreshold; 

% Stop filtering out the main portion of the picture
% The numbers refer to a range in the middle of the image
brightSpikes(173:199, 300:360) = 0;

% Filter/mask the spectrum and set it to zero.
frequencyImage(brightSpikes) = 0;
% Take log magnitude so we can see it better in the display.
amImage2 = log(abs(frequencyImage));
subplot(2, 3, 4);
imshow(amImage2, []);
title('Spikes zeroed out', 'FontSize', fontSize);

% Show final image with the mask applied

filteredImage = ifft2(fftshift(frequencyImage));
ampImage3 = abs(filteredImage);
subplot(2, 3, 5);
imshow(ampImage3, []);
title('Image filtered of Periodc Noise', 'FontSize', fontSize);
set(gcf, 'units','normalized','outerposition',[0 0 1 1]); % Maximize figure.