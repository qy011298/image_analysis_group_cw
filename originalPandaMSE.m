panda=imread('panda.bmp')
panda_noise=imread('pandanoise.bmp')
err=immse(panda,panda_noise)

fprintf('\n The mean-square-error is %0.4f\n', err)