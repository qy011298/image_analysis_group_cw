% Script that uses spactial filtering techniques to reduce the noise in the
% panda images.

panda=imread('panda.bmp');
panda_noise=imread('pandaNoise.bmp');
mse_original=immse(panda,panda_noise);
title_original=sprintf("Original Image, MSE: %0.2f", mse_original);

% Neighbourhood averaging:
% 3x3 neighbourhood
panda_3avrfilter=imfilter(panda_noise, fspecial('average', 3));
% 5x5 neighbourhood
panda_5avrfilter=imfilter(panda_noise, fspecial('average',5));
% 7x7 neighbourhood
panda_7avrfilter=imfilter(panda_noise, fspecial('average',7));
mse_3avrfilter=immse(panda,panda_3avrfilter);
mse_5avrfilter=immse(panda,panda_5avrfilter);
mse_7avrfilter=immse(panda,panda_7avrfilter);
title_3avrfilt=sprintf("Average Filter 3x3, MSE: %0.2f", mse_3avrfilter);
title_5avrfilt=sprintf("Average Filter 5x5, MSE: %0.2f", mse_5avrfilter);
title_7avrfilt=sprintf("Average Filter 7x7, MSE: %0.2f", mse_7avrfilter);
figure('Name', "Neighbourhood Filter");
subplot(2,2,1), imshow(panda_noise), title(title_original);
subplot(2,2,2), imshow(panda_3avrfilter), title(title_3avrfilt);
subplot(2,2,3), imshow(panda_5avrfilter), title(title_5avrfilt);
subplot(2,2,4), imshow(panda_7avrfilter), title(title_7avrfilt);


% Median Filtering / edge preserving
% 3x3 median filter
panda_3medfilt=medfilt2(panda_noise);
mse_3medfilter=immse(panda,panda_3medfilt);
title_3medfilt=sprintf("Median Filter 3x3, MSE: %0.2f", mse_3medfilter);
% 5x5 median filter
panda_5medfilt=medfilt2(panda_noise, [5,5]);
mse_5medfilter=immse(panda,panda_5medfilt);
title_5medfilt=sprintf("Median Filter 5x5, MSE: %0.2f", mse_5medfilter);
% 7x7 median filter
panda_7medfilt=medfilt2(panda_noise, [7,7]);
mse_7medfilter=immse(panda,panda_7medfilt);
title_7medfilt=sprintf("Median Filter 7x7, MSE: %0.2f", mse_7medfilter);

figure('Name', "Median Filtering");
subplot(2,2,1), imshow(panda_noise), title(title_original);
subplot(2,2,2), imshow(panda_3medfilt), title(title_3medfilt);
subplot(2,2,3), imshow(panda_5medfilt), title(title_5medfilt);
subplot(2,2,4), imshow(panda_7medfilt), title(title_7medfilt);