peng=imread('Penguin.bmp')
peng_noise=imread('PenguinNoise.bmp')
err=immse(peng,peng_noise)

fprintf('\n The mean-square-error is %0.4f\n', err)